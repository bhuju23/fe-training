let mix = require('laravel-mix');
let getMixMode = function () {
  let mode = 'dev';

  if (process.argv.includes('--watch')) {
    mode = 'watch';
  }

  if (process.argv.includes('--hot')) {
    mode = 'hot';
  }

  if (mix.inProduction()) {
    mode = 'production';
  }
  return mode;
};

let removeLeadingSlash = (text) => {
  return text.replace(/^\//, '');
};

// noinspection WebpackConfigHighlighting
module.exports = {
  getMixMode,
  removeLeadingSlash
};
