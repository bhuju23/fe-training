/* eslint-disable */
let mix = require('laravel-mix');
const settings = require('../../mix.settings');
const utils = require('./_utils');

/*
  | Setup up the distribution path 
*/

let publicPath = `${settings.paths.dist.baseFolder}/${settings.paths.dist.assetsFolder}/`;
console.log('--- Start --- Compiling Assets in ' + publicPath + 'folder. ---');
console.log('--- Mode --- ' + utils.getMixMode() + ' ---');

mix.setPublicPath(publicPath);

let styles = require('../_tasks/styles.js');
styles.init(settings);

let scripts = require('../_tasks/scripts.js');
scripts.init(settings);

let fonts = require('../_tasks/fonts.js');
fonts.init(settings);

let images = require('../_tasks/images.js');
images.init(settings);

let videos = require('../_tasks/videos.js');
videos.init(settings);

let versioning = require('../_tasks/versioning.js');
versioning.init(settings);

if (utils.getMixMode() === 'hot') {
  let hmr = require('../_tasks/hmr.js');
  hmr.init(settings);
}

mix.webpackConfig({
  stats: {
    children: true
  }
});

// mix.disableNotifications();
mix.disableSuccessNotifications();

mix.then((stats) => {
  // console.log(stats);
  // console.log(Object.keys(stats));
});
// // TODO
// // [x] Twig
// //    [x] Local Json Server
// // [x] Styles
// //    [x] Purge
// //    [x] Critical
// //    [-] stylelint
// // [x] Scripts
// //    [x] Pollyfills
// //    [x] Eslint
// // [x] Versioning
// // [x] HRM
// // [x] Prettier
// // [x] Commit Hooks
// // [-] Split ( Modern & legacy Build)
