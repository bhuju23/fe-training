/* eslint-disable */
const mix = require('laravel-mix');
const settings = require('../../mix.settings');
const critical = require('critical');

let publicPath = `${settings.paths.dist.baseFolder}/${settings.paths.dist.assetsFolder}/`;
mix.setPublicPath(publicPath);

mix.then(() => {
  console.log('--- Generating Ciritcal CSS ---');
  console.log(settings.criticalCss);
  critical.generate({
    base: './',
    src: '/fe-web/html/' + settings.criticalCss.criticalPage, // we should create critical.html
    css: ['./fe-web/assets/dist/css/main.css'],
    target: './fe-src/templates/_layouts/critical.css',
    dimensions: [
      {
        height: 480,
        width: 900
      },
      {
        height: 2400,
        width: 10000
      }
    ],
    penthouse: {
      blockJSRequests: true
    }
  });
  critical.generate({
    base: './',
    src: '/fe-web/html/' + settings.criticalCss.criticalPage, // we should create critical.html
    css: ['./fe-web/assets/dist/css/main.css'],
    target: './fe-src/css/critical.css',
    dimensions: [
      {
        height: 480,
        width: 900
      },
      {
        height: 2400,
        width: 10000
      }
    ],
    penthouse: {
      blockJSRequests: true
    }
  });
});
