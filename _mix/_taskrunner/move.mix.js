/* eslint-disable */
const mix = require('laravel-mix');
const settings = require('../../mix.settings');
const utils = require('./_utils');

let publicPath = `${settings.moveFiles.dest}`;

mix.copyDirectory(settings.moveFiles.src, settings.moveFiles.dest);

mix.then(() => {
  console.log(
    '--- Complete --- Compiling HTML in ' + publicPath + 'folder. ---'
  );
});
