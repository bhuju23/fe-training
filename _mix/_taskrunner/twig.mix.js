/* eslint-disable */
const mix = require('laravel-mix');
const settings = require('../../mix.settings');
const utils = require('./_utils');

// TODO: Clean Generated files via Prettier after compilation

let publicPath = `${settings.paths.dist.baseFolder}/${settings.paths.dist.htmlFolder}/`;
console.log('--- Start -- Compiling HTML in ' + publicPath + 'folder. ---');
console.log('--- Mode --- ' + utils.getMixMode() + ' ---');

mix.setPublicPath(publicPath);

let templates = require('../_tasks/twig-templates.js');
templates.init(settings);

if (utils.getMixMode() === 'watch' && settings.twig.browsersync.enabled) {
  console.log('--- Run Browser Sync --- ');
  let browserSync = require('../_tasks/browserSync.js');
  browserSync.init(settings);
}

if (utils.getMixMode() === 'hot') {
  let hmr = require('../_tasks/hmr-twig.js');
  hmr.init(settings);
}

mix.webpackConfig({
  stats: {
    children: true
  }
});

// mix.disableNotifications();
mix.disableSuccessNotifications();

mix.then((stats) => {
  console.log(
    '--- Complete --- Compiling HTML in ' + publicPath + 'folder. ---'
  );
  console.log('--- Moving Critial CSS file to assets folder  ---');
});
