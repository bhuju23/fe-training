let mix = require('laravel-mix');
const settings = require('../../mix.settings');
const utils = require('../_taskrunner/_utils');

let init = function () {
  /*
   * BrowserSync
   *
   */
  if (utils.getMixMode() === 'watch') {
    console.log(
      '--- Running BrowserSync @ -- ',
      settings.paths.dist.baseFolder
    );
    mix.browserSync({
      server: {
        baseDir: [`${settings.paths.dist.baseFolder}`],
        // directory: true,
        index: 'index.html',
        serveStaticOptions: {
          extensions: ['html']
        }
      },
      middleware: function (req, res, next) {
        console.log(req.url);
        if (req.url.includes('assets/dist')) {
          req.url = `${req.url}`;
        } else {
          req.url = `/html/${req.url}`;
        }

        return next();
      },
      port: settings.twig.browsersync.port,
      https: settings.twig.browsersync.https
    });
  }

  // if (utils.getMixMode() === 'hot') {
  //   console.log(
  //     '--- Running BrowserSync @ -- ',
  //     settings.paths.dist.baseFolder
  //   );
  //   mix.browserSync({
  //     files: [`${settings.paths.src.baseFolder}/templates/**/*`],
  //     server: {
  //       baseDir: [`${settings.paths.dist.baseFolder}`],
  //       directory: true
  //     },
  //     https: https
  //   });
  // }
};

module.exports = {
  init
};
