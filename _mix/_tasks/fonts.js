let mix = require('laravel-mix');

let init = function(projectSettings) {
  /*
   * Fonts
   *
   */
  (function initCopyFonts(mix) {
    mix.copyDirectory(
      `${projectSettings.paths.src.baseFolder}/fonts`,
      `${projectSettings.paths.dist.baseFolder}/${projectSettings.paths.dist.assetsFolder}/fonts`
    );
  })(mix);
};

module.exports = {
  init
};
