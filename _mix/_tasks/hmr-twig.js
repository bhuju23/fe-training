let mix = require('laravel-mix');

let init = function (projectSettings) {
  let hmrOptions = {
    https: false,
    host: 'localhost',
    port: 10002
  };

  hmrOptions = projectSettings.hmr.html.options;

  if (projectSettings.hmr.https) {
    hmrOptions.https = true;
  }

  if (projectSettings.hmr.html.mainPage) {
    let http = projectSettings.hmr.https ? 'https' : 'http';

    let mainPage = `${http}://${projectSettings.hmr.html.options.host}:${projectSettings.hmr.html.options.port}/${projectSettings.hmr.html.mainPage}`;

    mix.webpackConfig({
      devServer: {
        historyApiFallback: {
          rewrites: [
            {
              from: '/*$',
              to: function (context) {
                console.log('Rewriting', context.parsedUrl.pathname);
                return context.parsedUrl.pathname + '.html';
              }
            }
          ]
        },
        open: [mainPage],
        // dev: {
        //   // writeToDisk: true,
        // },
        static: [
          // Simple example
          {
            directory: projectSettings.twig.paths.staticImageFolder,
            publicPath: '/assets/dist/images'
          },
          {
            directory: projectSettings.twig.paths.staticFontsFolder,
            publicPath: '/assets/dist/fonts'
          },
          {
            directory: projectSettings.twig.paths.staticFontsFolder,
            publicPath: '/assets/dist/fonts'
          }

          // projectSettings.twig.paths.staticFolder
          // Complex example
          // {
          //   directory: '/assets/dist',
          //   // staticOptions: {},
          //   // // Don't be confused with `dev.publicPath`, it is `publicPath` for static directory
          //   // // Can be:
          //   publicPath: ['/images/', '/fonts/'],
          //   // publicPath: '/static-public-path/',
          //   // // Can be:
          //   // // serveIndex: {} (options for the `serveIndex` option you can find https://github.com/expressjs/serve-index)
          //   // serveIndex: true,
          //   // // Can be:
          //   // // watch: {} (options for the `watch` option you can find https://github.com/paulmillr/chokidar)
          //   // watch: true,
          // }
        ]
      }
    });
  }

  if (projectSettings.hmr.html.browsersync) {
    let http = projectSettings.hmr.https ? 'https' : 'http';
    let url = `${http}://${projectSettings.hmr.html.options.host}:${projectSettings.hmr.html.options.port}/${projectSettings.hmr.html.mainPage}`;
    mix.browserSync({
      proxy: url
    });
  }

  // mix.webpackConfig({
  //   watchOptions: {
  //     poll: 1
  //   },
  //   devServer: {
  //     dev: {
  //       // writeToDisk: true
  //     }
  //   }
  // });

  mix.options({
    hmr: true,
    hmrOptions: hmrOptions
  });

  mix.then(() => {
    console.log('--- HMR enabled with --------', hmrOptions);
  });
};

module.exports = {
  init
};
