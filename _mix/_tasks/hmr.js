let mix = require('laravel-mix');

let init = function (projectSettings) {
  let hmrOptions = {
    https: false,
    host: 'localhost',
    port: 10001
  };

  hmrOptions = projectSettings.hmr.assets.options;

  if (projectSettings.hmr.https) {
    hmrOptions.https = true;
  }

  if (projectSettings.hmr.https) {
    let http = projectSettings.hmr.https ? 'https' : 'http';
    let mainPage = `${http}://${projectSettings.hmr.assets.options.host}:${projectSettings.hmr.assets.options.port}/webpack-dev-server`;
    console.log('mainPage', mainPage);
    //https://github.com/webpack/webpack-dev-server/issues/2958
    mix.webpackConfig({
      devServer: {
        open: [mainPage],
        static: [
          // Simple example
          projectSettings.paths.dist.baseFolder
          // Complex example
          // {
          //   directory: '/assets/dist',
          //   // staticOptions: {},
          //   // // Don't be confused with `dev.publicPath`, it is `publicPath` for static directory
          //   // // Can be:
          //   publicPath: ['/images/', '/fonts/'],
          //   // publicPath: '/static-public-path/',
          //   // // Can be:
          //   // // serveIndex: {} (options for the `serveIndex` option you can find https://github.com/expressjs/serve-index)
          //   // serveIndex: true,
          //   // // Can be:
          //   // // watch: {} (options for the `watch` option you can find https://github.com/paulmillr/chokidar)
          //   // watch: true,
          // }
        ],
        devMiddleware: {
          // writeToDisk: true
        }
      }
    });
  }

  mix.options({
    hmr: true,
    hmrOptions: hmrOptions
  });

  mix.then(() => {
    console.log('--- HMR enabled with --------', hmrOptions);
  });
};

module.exports = {
  init
};
