let mix = require('laravel-mix');

let init = function (projectSettings) {
  mix.copyDirectory(
    `${projectSettings.paths.src.baseFolder}/images/`,
    `${projectSettings.paths.dist.baseFolder}/${projectSettings.paths.dist.assetsFolder}/images/`
  );
};

module.exports = {
  init
};
