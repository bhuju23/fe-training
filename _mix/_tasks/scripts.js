let mix = require('laravel-mix');
const utils = require('../_taskrunner/_utils');

/**
 * Task that created pollyfilled version of JS for older browsers
 */
let usePolyfills = function () {
  require('laravel-mix-polyfill');
  mix.polyfill({
    enabled: true,
    useBuiltIns: 'usage',
    targets: 'IE 11'
  });
};

let init = function (projectSettings) {
  /**
   * Autoload JS modules on all the JS files
   */
  mix.autoload({});

  projectSettings.paths.src.js.forEach((item) => {
    mix
      .js(
        `${projectSettings.paths.src.baseFolder}/js/` + item.src,
        'js/' + item.dest
      )
      .vue()
      .webpackConfig((webpack) => {
        /**
         * Public path needs to be set correctly for webpack dynamic-import to work properly
         */
        let publicPath = projectSettings.urls.publicPath();
        if (publicPath.charAt(0) === '/') {
          publicPath = publicPath.substring(1);
        }
        publicPath = `/${publicPath}`;

        /**
         * Setting Public path HMR mode
         */

        if (utils.getMixMode() === 'hot') {
          const http = projectSettings.hmr.https ? 'https' : 'http';
          publicPath = `${http}://${projectSettings.hmr.assets.options.host}:${projectSettings.hmr.assets.options.port}/${projectSettings.paths.dist.assetsFolder}/`;
        }
        console.log('Webpack Public Path : ', publicPath);

        /**
         * Update the chunknames for dynmaic imports
         */

        return {
          output: {
            chunkFilename: (pathData) => {
              let jsFileName = 'js/chunks/[name].[chunkhash].js';
              if (pathData.chunk.name.includes('vendor') === true) {
                jsFileName = 'js/[name].js';
              }
              return jsFileName;
            },
            publicPath: publicPath
          }
        };
      });
    // .extract(['vue'], 'vendor/vue.js');
    /**
     * Extracting Vendor files
     */
  });

  /**
   * Add any webpack configs that your project needs
   */
  // mix.webpackConfig({
  //   // resolve: {
  //   //   modules: [path.resolve(__dirname, 'node_modules/vue/dist')]
  //   // }
  // });

  /**
   * Use it for JS Pollyfills
   */

  let enablePollyfill = projectSettings.js.pollyfill.enabled || false;
  if (enablePollyfill) {
    usePolyfills();
  }

  if (mix.inProduction()) {
    mix.options({
      terser: {
        terserOptions: {
          compress: {
            drop_console: true
          }
        }
      }
    });
  }
};

module.exports = {
  init
};
