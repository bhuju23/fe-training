let mix = require('laravel-mix');
let tailwindcss = require('tailwindcss');

// /*
//  * Styles
//  * Uses Sass + TailwindCss 3
//  */

// /**
//  * https://github.com/tailwindlabs/tailwindcss/issues/4838
//  */

// let init = function (projectSettings) {
//   projectSettings.paths.src.css.forEach((item) => {
//     mix
//       .sass(
//         `${projectSettings.paths.src.baseFolder}/scss/` + item.src,
//         'css/' + item.dest
//       )
//       .options({
//         processCssUrls: false,
//         autoprefixer: false,
//         postCss: [tailwindcss('./tailwind.config.js')]
//       });
//   });
//   mix.css(`${projectSettings.paths.src.baseFolder}/scss/critical.css`, 'css/');
// };

/*
 * Styles
 * Uses PostCss + TailwindCss 3
 */

/**
 * https://github.com/tailwindlabs/tailwindcss/issues/4838
 * https://tailwindcss.com/docs/guides/laravel
 */
let init = function (projectSettings) {
  projectSettings.paths.src.css.forEach((item) => {
    mix
      .postCss(
        `${projectSettings.paths.src.baseFolder}/css/` + item.src,
        'css/' + item.dest,
        [
          require('postcss-import'),
          require('tailwindcss/nesting'), // uses https://github.com/postcss/postcss-nested
          require('tailwindcss')
        ]
      )
      .options({
        processCssUrls: false,
        autoprefixer: false
      });
  });
  mix.css(`${projectSettings.paths.src.baseFolder}/css/critical.css`, 'css/');
};

module.exports = {
  init
};
