const marked = require('marked');
const path = require('path');
const fs = require('fs');

let init = function (Twig, projectSettings) {
  return Twig.exports.extendTag({
    type: 'markdown',
    regex: /^markdown\s+(.+)$/,
    next: [],
    open: true,
    compile: function (token) {
      var expression = token.match[1];

      token.stack = Twig.expression.compile.apply(this, [
        {
          type: Twig.expression.type.expression,
          value: expression
        }
      ]).stack;

      delete token.match;
      return token;
    },
    parse: function (token, context, chain) {
      console.log('--- Parsing Markdown ----');
      console.log('token: ', token.stack[0].value);
      console.log('context', context);
      console.log('--- // Parsing Markdown ----');
      let html = ' Error :  Could not find the file !!!!! ';
      try {
        html = marked(
          fs.readFileSync(
            projectSettings.twig.paths.markDownFolder + token.stack[0].value,
            'utf8'
          )
        );
      } catch (err) {
        // Here you get the error when the file was not found,
        // but you also get any other error
      }

      return {
        chain: false,
        output: html
        // output: Twig.expression.parse.apply(this, [token.stack, context])
      };
    }
  });
};

module.exports = {
  init
};
