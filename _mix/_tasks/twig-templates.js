/* eslint-disable */
let mix = require('laravel-mix');
const fs = require('fs');
// TODO: Update the plugin when new version with Webpack 5 support gets released
// require('laravel-mix-twig-to-html'); // from node_modules
require('../_plugins/laravel-mix-twig-to-html'); // local copy - temp fixes
const utils = require('../_taskrunner/_utils');

var path = require('path');
var jsonMerger = require('json-merger');

/**
 * Build Twig Templates
 */
const twigMarkDown = require('./twig-markdown.js');
let init = function (projectSettings) {
  if (mix.inProduction()) {
    console.log('Mix is in Production mode !!!');
  }

  let watchMarkdownFiles = (ctx) => {
    /**
     * hack for .md
     */

    var { sync } = require('glob');
    var allMarkdowns = sync(
      `${projectSettings.twig.paths.markDownFolder}/**/*.md`
    );

    let output = allMarkdowns.map((file) => {
      console.log('--- Watching Markdown ----');
      console.log('Markdown file: ', file);
      console.log('--- // Watching Markdown ----');
      ctx.context.addDependency(file);
    });
  };

  return mix.twigToHtml({
    files: projectSettings.twig.files,
    fileBase: `${projectSettings.paths.src.baseFolder}/templates/pages`,
    htmlOptions: {
      // minimize: mix.inProduction(),
      minimize: false,
      attributes: false
    },
    htmlWebpack: {
      inject: false,
      minify: false
    },
    twigOptions: {
      namespaces: projectSettings.twig.twigOptions.namespaces,
      functions: {
        mix(asset) {
          if (utils.getMixMode() === 'hot') {
            let http = 'http';
            if (projectSettings.hmr.https) {
              http = 'https';
            }
            let publicPath = `${http}://${projectSettings.hmr.assets.options.host}:${projectSettings.hmr.assets.options.port}`;
            asset = publicPath + asset;
            //asset = publicPath + asset.split('..')[1];
          } else {
            // console.log('Compiling: ', asset);
            const manifest = require(projectSettings.paths.dist.manifest);
            for (const item in manifest) {
              if (asset.includes(item)) {
                const head = asset.split(item);
                asset = head[0] + manifest[item];
              }
            }
          }
          return asset;
        }
      },
      data: (context) => {
        let ctx = { context };

        const sharedDataFile = projectSettings.twig.paths.dataFile;
        ctx.context.addDependency(sharedDataFile); // Force webpack to watch file
        const sharedData =
          ctx.context.fs.readJsonSync(sharedDataFile, { throws: false }) || {};
        // console.log('sharedData: ', sharedData);

        const pageDataFile =
          `${projectSettings.twig.paths.dataFolder}` +
          ctx.context.resourcePath
            .split(`${projectSettings.twig.paths.dataFolder}`)[1]
            .replace('.twig', '.data.json');

        let pageData = {};

        if (fs.existsSync(pageDataFile)) {
          console.log('File Exists!');
          pageData =
            ctx.context.fs.readJsonSync(pageDataFile, { throws: false }) || {};
          // console.log('pageData: ', pageData);
          ctx.context.addDependency(pageDataFile); // Force webpack to watch file
        }

        var result = jsonMerger.mergeObjects([sharedData, pageData]);
        // console.log(result);

        if (projectSettings.twig.parseMarkdown) {
          watchMarkdownFiles(ctx);
        }

        return result;
      },
      extend(Twig) {
        if (projectSettings.twig.parseMarkdown) {
          twigMarkDown.init(Twig, projectSettings);
        }
      }
      // debug: true
    }
  });
  // .webpackConfig(webpack => {
  //   /**
  //    * Update the chunknames for dynmaic imports
  //    */

  //   let http = projectSettings.hmr.https ? 'https' : 'http';
  //   let publicPath = `${http}://${projectSettings.hmr.html.options.host}:${projectSettings.hmr.html.options.port}/`;
  //   console.log('Webpack Public Path : ', publicPath);

  //   return {
  //     output: {
  //       publicPath: publicPath
  //     }
  //   };
  // });
};

module.exports = {
  init
};
