let mix = require('laravel-mix');
const utils = require('../_taskrunner/_utils');

/**
 * File Versioning
 */
function createHashedFile(projectSettings) {
  const convertToFileHash = require('laravel-mix-make-file-hash');
  convertToFileHash({
    publicPath: `./${projectSettings.paths.dist.baseFolder}/${projectSettings.paths.dist.assetsFolder}`,
    manifestFilePath: projectSettings.paths.dist.manifest,
    blacklist: ['/js/chunks/*', '/js/vendor/', '/images/*'],
    keepBlacklistedEntries: true
    // disableFileOperations: true
  });
}

let init = function(projectSettings) {
  let versioningEnbaled = true;
  let fileNameVersioningEnabled = projectSettings.versioning.fileName;

  if (versioningEnbaled) {
    mix.version();
  }

  if (fileNameVersioningEnabled) {
    if (utils.getMixMode() === 'watch') {
      return;
    }

    if (utils.getMixMode() === 'hot') {
      return;
    }

    mix.then(() => {
      /**
       * Use if filename transform is required.
       */

      console.log('--- filename based versioning ---');
      createHashedFile(projectSettings);
    });
  }
};

module.exports = {
  init
};
