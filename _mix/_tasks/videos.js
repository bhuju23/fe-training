let mix = require('laravel-mix');

let init = function(projectSettings) {
  mix.copyDirectory(
    `${projectSettings.paths.src.baseFolder}/videos/**`,
    `${projectSettings.paths.dist.baseFolder}/${projectSettings.paths.dist.assetsFolder}/videos`
  );
};

module.exports = {
  init
};
