import collapse from '@alpinejs/collapse';

window.Alpine.plugin(collapse);

export function accordion(config, Alpine) {
  return {
    open: false,
    isOpen() {
      return this.open;
    },
    toggle() {
      this.open = !this.open;
    }
  };
}
