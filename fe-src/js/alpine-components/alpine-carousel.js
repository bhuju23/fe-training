import Swiper, { Navigation } from 'swiper';
// import Swiper styles
// eslint-disable-next-line import/no-unresolved
import 'swiper/css';

export function carousel(config, Alpine) {
  return {
    config: config || {},
    mainSlider: null,
    init() {
      console.log('Running carousel Init!!!');
      const vm = this;
      const $nextBtn = vm.$refs.next;
      const $nav = vm.$refs.nav;
      let swiperNav = null;

      const swiperMain = new Swiper(vm.$el, {
        modules: [Navigation],
        loop: true,
        initialSlide: 0,
        slidesPerView: 1,
        spaceBetween: 0,
        navigation: {
          nextEl: $nextBtn
        },
        on: {
          init: function (mainSwiper) {
            console.log('swiperMain Initialized');

            if ($nav) {
              swiperNav = new Swiper($nav, {
                loop: true,
                autoHeight: true, // enable auto height
                slidesPerView: 1,
                spaceBetween: 0,
                initialSlide: 1,
                allowTouchMove: false,
                on: {
                  init: function (navSwiper) {
                    console.log('swiperNav Initialized');
                  }
                }
              });
            }
          }
        }
      });

      swiperMain.on('slideNextTransitionStart', (swiper) => {
        // console.log('moving to next slide');
        swiperNav.slideNext();
      });
      swiperMain.on('slidePrevTransitionStart', (swiper) => {
        // console.log('moving to prev slide');
        swiperNav.slidePrev();
      });

      swiperMain.on('slideChangeTransitionStart', (swiper) => {
        // console.log('Changing Slide');
        swiperNav.slideTo(swiperMain.activeIndex + 1);
        vm.currentSlide = swiperMain.activeIndex;

        if (vm.config.watchStore) {
          console.log('updateStore');
          vm.$store[`${vm.config.watchStore.name}`].updateCurrentSlide(
            vm.currentSlide
          );
        }
      });

      vm.mainSlider = swiperMain;

      /**
       * Syncing Two Different Carousel
       */

      if (vm.config.watchStore) {
        console.log(
          `Watching ${vm.config.watchStore.name}.${vm.config.watchStore.toWatch}`
        );

        Alpine.effect(() => {
          const currentSlide = Alpine.store(`${vm.config.watchStore.name}`)[
            vm.config.watchStore.toWatch
          ];

          vm.moveToSlide(currentSlide);
        });
      }
    },
    moveToNextSlide() {
      // console.log('moving to next slide');
      this.mainSlider.slideNext();
    },
    moveToPrevSlide() {
      // console.log('moving to prev slide');
      this.mainSlider.slidePrev();
    },
    moveToSlide(slideNumber) {
      console.log('moving to slide: ', slideNumber);
      this.mainSlider.slideTo(slideNumber);
    }
  };
}
