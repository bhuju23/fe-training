import {
  multiCategoryFilter,
  cloneArray,
  searchArray
} from '../utils/arrays/filterArray.js';

/**
 * The app reacts to changes on activeFilters[item] value
 * Starts with Config()
 * Run Init()
 *
 */

export function filtersMulti(config, Apline) {
  return {
    config: config || {},
    options: {
      multiSelect: true,
      pagination: {
        pageSize: 2
      }
    },
    pagination: {
      items: [],
      currentPage: 1
    },
    activeFilters: {},
    filteredList: [],
    jsFilterInitialized: false,
    isLoading: false,
    _initConfig() {
      const vm = this;
      console.log('Running Config Function');
      /**
       * Update Options & arrays passed
       */
      vm.options = { ...vm.options, ...vm.config.options };

      /**
       * Initialize all Items
       */

      if (!vm.config.allItems) {
        throw new Error('ALL items needs to be passed to filtersMulti() !');
      }

      if (vm.config.filters) {
        /**
         * create arrays of filters
         */
        Object.keys(vm.config.filters).forEach((filterItem) => {
          // console.log('creating array for filter ');
          vm.activeFilters[filterItem] = [];
        });
      } else {
        throw new Error('Filters needs to be passed to filtersMulti() !');
      }

      if (vm.config.activeFilters) {
        /**
         * create arrays of filters
         */
        Object.keys(vm.config.activeFilters).forEach((filterItem) => {
          // console.log('active filter item', filterItem);
          vm.activeFilters[filterItem] = vm.config.activeFilters[filterItem];
        });
      }

      /**
       * If there is a seach field
       */
      if (vm.config.options.hasSearchField) {
        vm.searchQuery = '';
      }
    },
    _setup() {
      const vm = this;
      vm._initConfig();
    },
    _getFilteredList() {
      const vm = this;
      console.log('Getting Filtered List');
      /**
       * 1. reset Filtered FilteredList
       * 2. create a temporaryList and asslign AllItems to it
       * 3. capture current state of activeFilters
       * 4. generate Filteredlist against (allItems, nodeToFilter , activeFilters)
       */
      vm.filteredList = [];
      let tempList = vm.config.allItems;
      const filtersToProcess = [];

      Object.keys(vm.activeFilters).forEach((filterItem) => {
        // console.log('filterItem:', filterItem);
        /**
         * check if the value of filter node is string
         */
        if (
          typeof vm.activeFilters[filterItem] === 'string' ||
          vm.activeFilters[filterItem] instanceof String
        ) {
          tempList = multiCategoryFilter(
            tempList, // array
            filterItem, // // eg. categories
            [
              {
                value: vm.activeFilters[filterItem] // String
              }
            ]
          );
        }
        /**
         * check if the value of filter node is Array
         */
        if (Array.isArray(vm.activeFilters[filterItem])) {
          // console.log(`vm.activeFilters[${filterItem}] is an Array`);

          const activeFilters = vm.activeFilters[filterItem].map((item) => ({
            value: item
          }));
          tempList = multiCategoryFilter(
            tempList, // array
            filterItem, // eg. categories
            activeFilters
          );
        }
      });
      // console.log('tempList :', tempList);
      vm.filteredList = tempList;
    },
    _resetPagination() {
      const vm = this;
      vm.pagination.items = new Array(0);
      vm.pagination.currentPage = 1;
    },
    _updatePaginatedItems() {
      const vm = this;

      // if (vm.options.pagination.enabled) {
      const { pageSize } = vm.options.pagination;
      // console.log('currentPage: ', vm.pagination.currentPage);
      // console.log('pageSize', pageSize);

      const offset = (vm.pagination.currentPage - 1) * pageSize;
      // console.log('offset', offset);
      // console.log(vm.filteredList);
      // console.log('vm.pagination.items', ...vm.pagination.items);

      const cloneOfFilteredList = cloneArray(vm.filteredList);

      const currentPageList = cloneOfFilteredList.slice(
        offset,
        offset + pageSize
      );

      vm.$nextTick(() => {
        vm.pagination.items.forEach((item) => {
          const tempItem = item;
          tempItem.show = false;
        });
      });

      /** *
       * To Fix list
       */

      vm.pagination.items = [...vm.pagination.items, ...currentPageList];

      // console.log(vm.pagination.items);

      /**
       * Transition Hack
       */
      vm.$nextTick(() => {
        vm.pagination.items.forEach((item) => {
          const tempItem = item;
          tempItem.show = true;
        });
      });
      // }
    },
    showLoadMore() {
      const vm = this;
      if (vm.getLengthOfPaginatedItems() >= vm.getLengthOfFilteredItems()) {
        return false;
      }
      return true;
    },
    loadMore() {
      const vm = this;
      vm.isLoading = true;
      vm.pagination.currentPage = 1 + vm.pagination.currentPage;
      vm._updatePaginatedItems();
      vm.isLoading = false;
    },
    getLengthOfFilteredItems() {
      const vm = this;
      return vm.filteredList.length;
    },
    getLengthOfPaginatedItems() {
      const vm = this;
      return vm.pagination.items.length;
    },
    _resetSubCategories(filterKey) {
      const vm = this;
      if (vm.activeFilters) {
        /**
         * create arrays of filters
         */

        Object.keys(vm.activeFilters).forEach((filterItem) => {
          // console.log(filterItem.toString());
          // console.log(filterKey);
          if (filterItem.toString() !== filterKey) {
            if (Array.isArray(vm.activeFilters[filterItem])) {
              console.log('Resetting activeFilter');
              vm.activeFilters[filterItem] = ['All'];
            } else {
              vm.activeFilters[filterItem] = 'All';
            }
          }
        });
      }
    },
    handleChangeOnSingle(e, categoryType) {
      const vm = this;

      if (categoryType && categoryType.type === 'main') {
        vm._resetSubCategories(categoryType.id);
      }

      vm._getFilteredList();
      vm._resetPagination();
      vm._updatePaginatedItems();
    },
    handleChangeOnMulti(e, categoryType) {
      const vm = this;
      const selectedValue = e.target.value;

      if (selectedValue.toLowerCase() === 'all' && e.target.checked) {
        console.log();
        vm.activeFilters[categoryType.id] = ['All'];
      } else {
        vm.activeFilters[categoryType.id] = vm.activeFilters[
          categoryType.id
        ].filter((item) => item.toLowerCase() !== 'all');
        // console.log('removing all', vm.activeFilters[categoryType.id]);
      }

      // reseting to all
      if (
        vm.activeFilters[categoryType.id].length ===
        vm.config.filters[categoryType.id].length - 1
      ) {
        vm.activeFilters[categoryType.id] = ['All'];
      }

      // if nothing has been selected
      if (vm.activeFilters[categoryType.id].length === 0) {
        vm.activeFilters[categoryType.id] = ['All'];
      }

      if (categoryType && categoryType.type === 'main') {
        vm._resetSubCategories(categoryType.id);
      }
      vm._getFilteredList();
      vm._resetPagination();
      vm._updatePaginatedItems();
    },

    handleSearch(e) {
      const vm = this;
      const query = vm.searchQuery;

      const results = searchArray(
        vm.filteredList,
        vm.config.options.search.nodes,
        query
      );
      vm.filteredList = results;
      vm.$nextTick(() => {
        vm._resetPagination();
        vm._updatePaginatedItems();
      });
    },

    init() {
      const vm = this;
      vm._setup();
      vm.$nextTick(() => {
        vm._getFilteredList();
        vm._resetPagination();
        vm._updatePaginatedItems();
      });
    }
  };
}
