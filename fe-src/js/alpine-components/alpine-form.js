import { Iodine } from '@kingshott/iodine/src/iodine.js';

const axios = require('axios');

export function formCapture(Alpine) {
  return {
    formData: {},
    allFormElements: null,
    formIsValid: false,
    iodine: null,
    postState: {
      isPosting: false,
      response: {
        status: '',
        message: '',
        data: {}
      }
    },
    postMessage: {
      message: {},
      data: {}
    },
    _bindFocus() {
      const vm = this;
      const $firstInput = vm.$el.querySelector('input,select,textarea');
      $firstInput.addEventListener('focus', (e) => {
        this._addRecaptcha();
      });
    },
    _addRecaptcha() {
      const vm = this;

      // if (!vm.formData.recaptcha) {

      // }

      vm.formData.recaptcha = {
        name: 'recaptcha',
        isValid: false,
        errorMessage: '',
        fieldName: 'Robot validation',
        rules: ['required']
      };

      vm.$nextTick(() => {
        const scriptTag = vm.$refs.recaptchaScript;
        // add recpatcha in form Data

        const verifyCallback = function (response) {
          vm.formData.recaptcha.isValid = true;
          vm.formData.recaptcha.value = response;
          vm.formData.recaptcha.errorMessage = '';
        };

        window.onRepactchaLoad = () => {
          // eslint-disable-next-line no-undef
          window.grecaptcha.render(vm.$refs.recaptchaContainer, {
            sitekey: '6Lev1x0eAAAAAEN4SLejM3t9koiEsEHL6aPsxN_O',
            callback: verifyCallback
          });
        };

        scriptTag.setAttribute(
          'src',
          'https://www.google.com/recaptcha/api.js?onload=onRepactchaLoad&render=explicit'
        );
      });
    },
    _resetForm() {
      const vm = this;
      window.grecaptcha.reset(vm.$refs.recaptchaContainer);
      Object.keys(vm.formData).forEach((key) => {
        const item = vm.formData[key];
        item.value = null;
        item.isValid = false;
      });
    },
    postForm() {
      const vm = this;
      const data = {};
      Object.keys(vm.formData).forEach((key) => {
        data[key] = vm.formData[key].value || 'empty';
      });
      vm.postState.isPosting = true;

      axios
        .post(`https://61e63ed6ce3a2d001735902a.mockapi.io/api/subscribe`, data)
        .then((response) => {
          console.log(response.data);

          vm.postState.response.status = 'success';
          vm.postState.response.message = response.data.message;
          vm.postState.response.data = JSON.stringify(response.data.data);
          vm.postState.isPosting = false;
          vm._resetForm();
        })
        .catch((error) => {
          // handle error
          console.log(error);
          vm.postState.isPosting = false;
        });
    },
    validate(event) {
      const vm = this;
      const fieldName = event.target.getAttribute('name');
      const formItem = vm.formData[fieldName];
      let error = '';
      if (vm.iodine.isArray(formItem.value)) {
        error = vm.iodine.is(formItem.value.join(','), formItem.rules); // true
      } else {
        error = vm.iodine.is(formItem.value, formItem.rules); // true
      }
      if (error !== true) {
        // console.log(vm.formData[item.name]);

        formItem.errorMessage = vm.iodine.getErrorMessage(error, {
          field: formItem.fieldName,
          param: ''
        }); // string
      } else {
        formItem.errorMessage = '';
        formItem.isValid = true;
      }
      console.log('error', error);
      console.log('item', formItem);
    },
    _validateForm() {
      const vm = this;
      Object.keys(vm.formData).forEach((key) => {
        // console.log(key, vm.formData[key]);
        const item = vm.formData[key];
        if (item.rules) {
          let error = '';
          if (Array.isArray(item.value)) {
            error = vm.iodine.is(item.value.join(','), item.rules); // true
          } else {
            error = vm.iodine.is(item.value, item.rules); // true
          }

          if (error !== true) {
            // console.log(vm.formData[item.name]);
            vm.formData[item.name].errorMessage = vm.iodine.getErrorMessage(
              error,
              {
                field: item.fieldName,
                param: ''
              }
            ); // string
          }
        }
      });
    },
    _checkFormValid() {
      const vm = this;
      let validity = [];
      let isValid = false;
      Object.keys(vm.formData).forEach((key) => {
        validity.push(vm.formData[key].isValid);
      });
      validity = [...new Set(validity)];

      if (validity.length > 1) {
        isValid = false;
      }

      if (validity.length === 1) {
        if (validity[0]) {
          isValid = true;
        }
      }

      return isValid;
    },
    _setupForm() {
      const vm = this;
      /**
       * Set up Form Data for all input elements
       */
      vm.allFormElements.forEach((item) => {
        const fieldName = item.getAttribute('name');
        // console.log('Setting up', item);
        vm.formData[fieldName] = {
          name: fieldName,
          isValid: (() => {
            if (item.dataset.rules) {
              const validationRules = item.dataset.rules;
              if (validationRules.includes('optional')) {
                return true;
              }
              return false;
            }
            return true;
          })(),
          errorMessage: '',
          fieldName: item.getAttribute('data-rules-fieldName'),
          rules: (() => {
            if (item.dataset.rules) {
              return JSON.parse(item.dataset.rules);
            }
            return [];
          })()
        };
        /**
         * Update existing Value
         */

        if (item.type) {
          if (
            item.type === 'text' ||
            item.type === 'email' ||
            item.tagName === 'TEXTAREA'
          ) {
            if (item.value) {
              vm.formData[fieldName].value = item.value;
              vm.formData[fieldName].isValid = true;
            }
          }

          if (item.type === 'radio') {
            const checkedValue = document.querySelector(
              `input[name=${fieldName}]:checked`
            );
            if (checkedValue) {
              vm.formData[fieldName].isValid = true;
              vm.formData[fieldName].value = checkedValue.value;
            }
          }

          if (item.type === 'checkbox') {
            // eslint-disable-next-line no-param-reassign
            vm.formData[fieldName].value = [];
            const checkedItems = document.querySelectorAll(
              `input[name=${fieldName}]:checked`
            );

            if (checkedItems.length) {
              vm.formData[fieldName].isValid = true;
              checkedItems.forEach((checkedItem) => {
                // console.log(checkedItem.value);
                vm.formData[fieldName].value.push(checkedItem.value);
              });
            }
          }

          if (item.tagName === 'SELECT') {
            const selectedOption = document.querySelector(
              `select[name=${fieldName}] option:checked`
            );
            if (selectedOption.value) {
              vm.formData[fieldName].isValid = true;
              vm.formData[fieldName].value = selectedOption.value;
            }
          }
        }
      });

      /**
       * Init Iodine
       */

      // enable Iodine
      const iodine = new Iodine();
      iodine.setErrorMessage('required', '[FIELD] is required.'); // English
      iodine.setErrorMessage('email', '[FIELD] must be valid.'); // English
      vm.iodine = iodine;
      window.iodine = iodine;
    },
    submitForm(e) {
      const vm = this;
      e.preventDefault();
      console.log('FormData: ', vm.formData);
      // console.log('Form is Valid: ', vm.checkFormValid());
      if (vm.postState.isPosting !== true) {
        if (!vm._checkFormValid()) {
          vm._validateForm();
        } else {
          vm.postForm();
        }
      }
    },
    init() {
      const vm = this;
      const allFormElements = vm.$el.querySelectorAll('[x-input]');
      vm.allFormElements = allFormElements;
      // console.log(vm.allFormElements);
      vm._setupForm();
      vm._bindFocus();
    }
  };
}
