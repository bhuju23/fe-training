import {
  initComponents,
  findAplineComponentsOnNode
} from '../utils/loaders/alpineLoader.js';

export function lazy(config, Alpine) {
  return {
    loaded: false,
    loadContent() {
      const vm = this;
      const $component = vm.$refs.lazyComponent.content;
      const $alpinecomponents = findAplineComponentsOnNode($component);

      if (!vm.loaded) {
        initComponents($alpinecomponents).then(() => {
          vm.loaded = true;
        });
      }
    },
    init() {}
  };
}
