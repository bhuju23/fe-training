// https://61aee2972cdd90001740764e.mockapi.io/api/blog?page=1&limit=4

const axios = require('axios');

export function loadmore(config, Alpine) {
  return {
    items: [],
    noMoreItemsToShow: false,
    isLoading: false,
    options: {
      page: 1,
      limit: 2
    },

    showLoadMore() {
      return this.noMoreItemsToShow;
    },
    setup(passedConfig) {
      const vm = this;
      vm.options = { ...vm.options, ...passedConfig };
      console.log(vm.options);
    },
    toggleLoading() {
      const vm = this;
      vm.isLoading = !vm.isLoading;
    },
    loadMore() {
      const vm = this;
      if (!vm.isLoading) {
        vm.toggleLoading();
        axios
          .get(
            `https://61aee2972cdd90001740764e.mockapi.io/api/blog?page=${vm.options.page}&limit=${vm.options.limit}`
          )
          .then((response) => {
            // handle success
            console.log(response);
            vm.items = [...vm.items, ...response.data.items];
            vm.options.page += 1;
            if (vm.items.length >= response.data.total) {
              vm.noMoreItemsToShow = true;
            }
            vm.toggleLoading();
          })
          .catch((error) => {
            // handle error
            console.log(error);
          });
      }
    },
    init() {
      const vm = this;
      vm.setup(vm.config);
    }
  };
}
