export function modalCarouselSync(config, Alpine) {
  return {
    open: false,
    contentWrapper: null,
    modalContainer: null,
    faker: null,
    content: null,
    openedOnce: false,
    moveContent(container, content) {
      container.append(content);
    },
    hideModal() {
      const vm = this;

      /**
       * hide modal
       */
      if (vm.open) {
        vm.faker.style.width = 0;
        vm.faker.style.height = 0;
        vm.moveContent(vm.contentWrapper, vm.content);
      }

      vm.open = false;
    },
    showModal() {
      const vm = this;

      /**
       * add dummy content to prevent layout shift
       * */
      if (!vm.open) {
        const width = vm.content.offsetWidth;
        const height = vm.content.offsetHeight;
        vm.faker.style.width = width;
        vm.faker.style.height = height;
        vm.moveContent(vm.modalContainer, vm.content);
      }

      /**
       * show modal
       */
      vm.open = true;
    },
    init() {
      const vm = this;
      vm.$nextTick(() => {
        vm.contentWrapper = vm.$refs.contentWrapper;
        vm.modalContainer = vm.$refs.modalContainer;
        vm.content = vm.$refs.content;
        vm.faker = vm.$refs.faker;
      });
    }
  };
}
