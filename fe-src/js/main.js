import {
  loadAlpine,
  initComponents,
  findAplineComponentsOnNode
} from './utils/loaders/alpineLoader.js';

const manual = () => {
  /**
   * Load all components
   */
  initComponents([
    'alpine-accordion',
    'alpine-carousel',
    'alpine-filters-multi',
    'alpine-loadmore',
    'alpine-modal-carousel-sync',
    'alpine-modal',
    'alpine-lazy',
    'alpine-form'
  ]).then(() => {
    window.Alpine.start();
    console.log('Alpine has loaded ');
  });
};

const dynamic = () => {
  /**
   * Load components in page
   */
  const componentsInPapge = findAplineComponentsOnNode(document);
  initComponents(componentsInPapge).then(() => {
    window.Alpine.start();
  });
};

const main = () => {
  loadAlpine().then(() => {
    // manual();
    dynamic();
  });
};

main();

// // Accept HMR as per: https://webpack.js.org/api/hot-module-replacement#accept
if (module.hot) {
  module.hot.accept();
}
