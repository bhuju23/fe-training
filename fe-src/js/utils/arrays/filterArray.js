import arraySort from 'array-sort';
// import { str2bool } from '@/utils/stringToBoolean.js';

/**
 * Returns filtered array which matches the value property of objects passed
 *
 * @param {array} arr Array of objects to be filterd
 * @param {array} key  which node/key to filter
 * @param {array} filterItems  Array of objects to filter against
 * @returns {array} filtered Array
 */

const multiCategoryFilter = (arr, key, filterItems) => {
  // console.log('array to Filter:', arr);
  // console.log('filterItems: ', filterItems);
  function checkCategory(itemCategory, filterCategory) {
    // console.log('itemCategory: ', itemCategory);
    // console.log('filterCategory: ', filterCategory);
    // if (itemCategory.toLowerCase() === filterCategory.value.toLowerCase()) {
    //   console.log('Match');
    // }

    if (itemCategory.value === undefined) {
      throw new Error(`${key}.value must be an Present`);
    }

    return (
      itemCategory.value.toLowerCase() === filterCategory.value.toLowerCase()
    );
  }

  const filtered = arr.filter((item) => {
    if (!item[key]) {
      throw new Error(`item.${key} must be an Present`);
    }

    if (!Array.isArray(item[key])) {
      throw new Error(`item.${key} must be an Array`);
    }
    return item[key].some((category) =>
      filterItems.some((options) => checkCategory(category, options))
    );
  });

  return filtered;
};

/**
 *
 * @param {*} arr
 * @param {*} itemToRemove // pass the value property of the object to be removed
 * @returns // new filtered array
 */

const removeItemFormArray = (arr, itemToRemove) => {
  const filtered = arr.filter(
    (item) =>
      // console.log(`Removing ${item.value} from ${arr}`);
      item.value.toLowerCase() !== itemToRemove.value.toLowerCase()
  );

  // console.log('Filtered: ', filtered);
  return filtered;
};

const cloneArray = (arr) => JSON.parse(JSON.stringify(arr));

const sortArray = (array, options) => {
  let reverse = false;

  if (options.order === 'desc') {
    reverse = true;
  }
  // console.log(options);
  return arraySort(array, options.key, {
    reverse: reverse
  });
};

/**
 *
 * @param {*} arr - array of items
 * @param {*} nodes - array of nodes to check for query
 * @param {*} query - query string
 * @returns
 */
const searchArray = (arr, nodes, query) => {
  const searchQuery = query.toLowerCase();
  // console.log(searchQuery);

  /**
   *
   */
  const searchedArray = arr.filter((eachObj) => {
    // console.log("eachObj", eachObj);

    const newArray = nodes.some((eachKey) => {
      const objValue = eachObj[eachKey];
      // console.log("objValue", objValue);
      if (objValue === null || objValue === Boolean || objValue === undefined) {
        return false;
      }
      if (objValue.toString().toLowerCase().includes(searchQuery)) {
        return true;
      }
      return false;
    });

    return newArray;
  });

  console.log('searchedArray', searchedArray);

  return searchedArray;
};

const uniqueItems = (array, key) => {
  const unique = [...new Set(array.map((item) => item[key]))];
  return unique;
};

/**
 *
 */

const getNodesItems = (arr, node) => {
  const allItems = [];
  cloneArray(arr).forEach((item) => {
    item[node].forEach((nodeItem) => {
      allItems.push(nodeItem);
    });
  });

  let uniqueCategoryValues = uniqueItems(allItems, 'value');
  uniqueCategoryValues = arraySort(uniqueCategoryValues);
  const availableUniqueNodes = [];
  /** Moving All to front */
  if (uniqueCategoryValues.length) {
    const itemToFind = 'All';

    const foundIdx = uniqueCategoryValues.findIndex((el) => el === itemToFind);
    console.log(foundIdx);
    uniqueCategoryValues.splice(foundIdx, 1);
    uniqueCategoryValues.unshift(itemToFind);

    uniqueCategoryValues.forEach((item) => {
      const count = allItems.filter((filterItem) => {
        if (filterItem.value === item) {
          return true;
        }
        return false;
      });
      availableUniqueNodes.push({
        value: item,
        count: count.length
      });
    });
  }

  // console.log(availableUniqueNodes);
  return availableUniqueNodes;
};

export {
  multiCategoryFilter,
  removeItemFormArray,
  cloneArray,
  searchArray,
  getNodesItems
};
