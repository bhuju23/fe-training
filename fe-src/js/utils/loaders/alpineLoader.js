/**
 * Loads Single File
 */
const loadSingleAlpineFile = async (module) => {
  const promise = await import(
    /* webpackChunkName: "[request]" */ `../../alpine-components/${module}.js`
  );
  return promise;
};
/**
 * Loads AlpineJS
 */
const loadAlpine = async () => {
  const { default: Alpine } = await import(
    /* webpackChunkName: "alpinejs" */ 'alpinejs'
  );
  window.Alpine = Alpine;
  return Alpine;
};

/**
 * Adds Alpine Components to window
 */

const loadAlpineComponents = (modules) => {
  const promise = new Promise((resolve, reject) => {
    const promises = modules.map((mod) => loadSingleAlpineFile(mod));
    Promise.all(promises).then((values) => {
      values.forEach((module) => {
        Object.keys(module).forEach((attr) => {
          // console.log(module);
          // console.log(attr);
          const data = module[attr];
          // console.log(data);
          window.attr = data;
          // console.log(window.attr);
          window.Alpine.data(attr, (config) => data(config, window.Alpine));
        });
      });

      resolve();
    });
  });
  return promise;
};

/**
 * Finds Alpine compnents on DOM node (on DOM and inside Template Tags)
 */
const findAplineComponentsOnNode = ($el) => {
  // console.log('lets plan');

  let nodesToPrase = [];
  const alpineComponents = [];

  const initAlpineComponent = (component) => {
    alpineComponents.push(component);
    if (component.getAttribute('defer-x-data')) {
      component.setAttribute('x-data', component.getAttribute('defer-x-data'));
    }
  };

  const getChildTemplates = (el) => {
    const childTemplates = el.querySelectorAll('template');
    childTemplates.forEach((template) => {
      nodesToPrase.push(template);
      getChildTemplates(template.content);
    });
  };

  /*
   * [1] get all templates / child templates tags
   * [2] get all alpine components within these node
   * [3] load all the components
   */

  const alpineComponentsInDom = $el.querySelectorAll('[defer-x-data],[x-data]');

  alpineComponentsInDom.forEach((el) => {
    nodesToPrase.push(el);
    getChildTemplates(el);
  });

  nodesToPrase = [...new Set(nodesToPrase)];

  nodesToPrase.forEach((node) => {
    if (node.nodeName === 'TEMPLATE') {
      // console.log('Do Something');
      const comps = node.content.querySelectorAll('[defer-x-data],[x-data]');
      comps.forEach((comp) => {
        initAlpineComponent(comp);
      });
    } else {
      initAlpineComponent(node);
    }
  });

  let componentNameList = alpineComponents.map((comp) => {
    const componentId = comp.getAttribute('data-alpine-component');
    // console.log('componentId', componentId);
    return `alpine-${componentId}`;
  });

  componentNameList = componentNameList.filter((item) => {
    if (item === 'alpine-null') {
      return false;
    }
    return true;
  });

  // console.log(componentNameList);

  const uniqueComponentNameList = [...new Set(componentNameList)];

  // console.log(uniqueComponentNameList);

  return uniqueComponentNameList;
};

const initComponents = async (modules) => loadAlpineComponents(modules);

export { loadAlpine, initComponents, findAplineComponentsOnNode };

/**
 * Ref
 * - https://dev.to/keuller/build-modular-app-with-alpinejs-2ece
 * - https://github.com/alpinejs/alpine/issues/359
 */
