const path = require('path');
const utils = require('./_mix/_taskrunner/_utils.js');

/**
 * Creating a settings variable
 */
const settings = {};

/**
 * update paths
 */
settings.paths = {
  src: {
    baseFolder: './fe-src' // path to source folder,
  },
  dist: {
    baseFolder: './fe-web', // path to distribution folder
    /**
     * path to assest (js/css/images/fonts/etc) folder
     * make sure to reference correct path to assets folder on twig templates if you are making use of it
     * update the clean script on package.json to clean correct folders
     * */
    assetsFolder: utils.removeLeadingSlash('/assets/dist'),
    htmlFolder: 'html' // path to html template folder
  }
};

/**
 * Paths for  files to compile
 */
settings.paths.src.css = [
  {
    src: `main.css`,
    dest: 'main.css'
  },
  {
    src: `fonts.css`,
    dest: 'fonts.css'
  }
  // {
  //   src: `anotherMain.`,
  //   dest: 'anotherMain.js'
  // }
];

/**
 * Paths for js files to compile
 */
settings.paths.src.js = [
  {
    src: `main.js`,
    dest: 'main.js'
  },
  {
    src: `lazyLoad.js`,
    dest: 'lazyLoad.js'
  }
];

/**
 * path to mix-manifest.json file
 *  used for the following:
 *    - versioning
 *    - twig path compilation via mix function
 */
settings.paths.dist.manifest = path.join(
  __dirname,
  // eslint-disable-next-line no-use-before-define
  `${settings.paths.dist.baseFolder}/${settings.paths.dist.assetsFolder}/mix-manifest.json`
);

/**
 * update setting for JS
 */
settings.js = {
  pollyfill: {
    enabled: false
  },
  jquery: {
    enabled: true
  }
};

/**
 * Settings for urls
 * Public path needs to be set correctly for webpack dynamic-import to work properly
 * e.g. all the dynamic imports will be retrived from domain.com/dist/ folder
 */

settings.urls = {
  publicPath: () => `${settings.paths.dist.assetsFolder}/`
};

/**
 * update setting for Versioning
 */
settings.versioning = {
  fileName: false // default false
};

/**
 * update settings for Twig templates
 * we are using twig templates for static html generatation
 * assets generation can be used with/without twig templates
 */
settings.twig = {
  /**
   * files
   */
  files: [
    {
      template: `${settings.paths.src.baseFolder}/templates/pages/**/*.{twig,html}`
    }
  ],
  /**
   * Twig options
   */
  twigOptions: {
    namespaces: {
      root: path.join(__dirname, `${settings.paths.src.baseFolder}/templates`),
      sections: path.join(
        __dirname,
        `${settings.paths.src.baseFolder}/templates/_sections`
      ),
      components: path.join(
        __dirname,
        `${settings.paths.src.baseFolder}/templates/_components`
      ),
      alpineComponents: path.join(
        __dirname,
        `${settings.paths.src.baseFolder}/templates/_components/_alpine-components`
      )
    }
  },
  /**
   * Twig Data file for convinience
   * There is only one single data file for entire twig templates, hence use it with cation and lot of nested objects :)
   */
  paths: {
    dataFolder: path.join(
      __dirname,
      `${settings.paths.src.baseFolder}/templates/pages/`
    ),
    markDownFolder: path.join(
      __dirname,
      `${settings.paths.src.baseFolder}/templates/_data/_markdown/`
    ),
    dataFile: path.join(
      __dirname,
      `${settings.paths.src.baseFolder}/templates/_data/global.json`
    ),

    staticImageFolder: path.join(
      __dirname,
      `${settings.paths.src.baseFolder}/images`
    ),
    staticFontsFolder: path.join(
      __dirname,
      `${settings.paths.src.baseFolder}/fonts`
    )
  },
  browsersync: {
    enabled: true,
    port: 2000,
    https: true
  },
  parseMarkdown: false
};

/**
 *
 *
 */

settings.hmr = {
  /**
   * While using HMR all css / js + any files manipulated by webpack/laravel-mix will not be available on distribution folder as physical files
   */
  https: true,
  assets: {
    /**
     * When running this build withing VM
     * Use the ip address of Guest (VM)
     * Set port forwarding in HOST
     * You should be now able to access this server on HOST
     */
    options: {
      host: 'localhost',
      port: 3001
    }
  },
  html: {
    browsersync: false,
    /**
     * When running this build withing VM
     * Use the ip address of Guest (VM)
     * Set port forwarding in HOST
     * You should be now able to access this server on HOST
     * Alternatively, us can enbale browsersync to poxy and port forward the browsersync proxy port
     */
    options: {
      host: 'localhost',
      port: 3003
    },
    mainPage: 'index.html'
  }
};

/**
 * Copy Files / Folder
 * Useful when we want to move files from FE folder to CMS Template folders
 */

settings.moveFiles = {
  src: `${settings.paths.dist.baseFolder}/${settings.paths.dist.assetsFolder}`,
  dest: '../../src/HrBlock.Web/Content/assets'
};

/**
 * Criticalcss
 * we should create critical.html page which should consist all the compontents that can appear above the fold
 */

settings.criticalCss = {
  criticalPage: 'index.html'
};

module.exports = settings;
