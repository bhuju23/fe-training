module.exports = {
  // mode: 'jit',
  content: ['./fe-src/**/*.{html,twig,json,vue,js}'],
  theme: {
    extend: {
      colors: {
        primary: 'var(--brand-primary-color)',
        'primary-hover': 'var(--brand-primary-hover-color)',
        secondary: 'var(--brand-secondary-color)',
        gray: {
          100: '#F1F1F1',
          200: '#E5E5E5',
          300: '#838383',
          400: '#565656',
          500: '#202020'
        },
        error: '#FF0000',

        blue: '#1E40AF',
        current: 'currentColor'
      },
      stroke: theme => ({
        ...theme('colors')
      }),
      fill: theme => ({
        ...theme('colors')
      }),
      backgroundColor: theme => ({
        ...theme('colors')
      }),
      placeholderColor: theme => theme('colors'),
      spacing: {
        18: '4.5rem', // 72px
        68: '17rem', // 272px
        84: '22.5rem', // 360px
        video: '56.29%'
      },
      fontFamily: {
        brand: 'var(--brand-font-family)',
        sans: ['Inter', 'sans-serif']
      },
      fontSize: {
        xs: '0.6875rem',
        tiny: ['0.8125rem', '1.0625rem'],
        sm: ['.875rem', '1.375rem'],
        h2: ['1.5rem', '1.875rem'],
        h3: ['1.25rem', '1.75rem'],
        '4xl': [
          'var(--brand-mobile-heading-size)',
          {
            lineHeight: 'var(--brand-mobile-heading-leading)',
            letterSpacing: 'var(--brand-heading-letter-spacing)'
          }
        ],
        '5xl': [
          'var(--brand-desktop-heading-size)',
          {
            lineHeight: 'var(--brand-desktop-heading-leading)',
            letterSpacing: 'var(--brand-heading-letter-spacing)'
          }
        ],
        '6xl': '3.25rem', // 52px
        '7xl': '4.5rem' // 72px
      },
      maxWidth: {
        full: '100%',
        85: '21.25rem', // 340px
        105: '26.25rem', // 420px
        48: '12rem' // 192px
      },
      height: {
        19: '4.75rem', // 76px
        34: '8.5rem', // 136px
        59: '14.75rem', // 236px
        108: '27rem', // 432px
        112: '28rem', // 448px
        116: '29rem'
      },
      minHeight: {
        28: '7rem', // 112px
        78: '19.5rem' // 312px
      },
      maxHeight: { 9999: '9999px' },
      minWidth: {
        80: '20rem',
        28: '7rem',
        48: '12rem', // 192px for buttons
        51: '12.75rem' // 204px for primary buttons in banner
      },
      width: {
        13: '3.375rem', // 54px
        22: '5.5rem', // 88px
        38: '9.5rem', // 152px
        42: '10.5rem', // 168px
        45: '11.25rem', // 180px
        51: '12.75rem', // 204px
        73: '18.25rem', // 292px
        106: '26.5rem' // 422px
      },
      zIndex: {
        '-1': -1,
        '-99': -99,
        60: 60,
        70: 70,
        9999: 9999
      },
      borderWidth: {
        1: '1px',
        6: '6px'
      },
      transitionProperty: {
        left: 'left',
        icon: 'stroke, fill',
        hide: 'visibility, opacity',
        maxh: 'max-height'
      },
      boxShadow: {
        xs: '5px 5px 50px rgba(0, 0, 0, 0.1)',
        md: '0px 4px 4px rgba(0, 0, 0, 0.25)'
      },
      scale: {
        120: '1.2'
      },
      inset: {
        '-13': '-3.25rem', // 52px
        61: '15.25rem' // 244px
      }
    },
    screens: {
      xs: '375px',
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
      '2xl': '1536px'
    }
  },
  // variants: {
  //   extend: {
  //     display: ['responsive', 'group-hover'],
  //     width: ['responsive'],
  //     stroke: ['hover', 'focus', 'group-hover'],
  //     fill: ['hover', 'focus', 'group-hover'],
  //     transitionProperty: ['hover', 'focus', 'group-hover'],
  //     scale: ['active', 'group-hover']
  //   }
  // },
  plugins: [],
  corePlugins: {
    fontSmoothing: true
  }
};
